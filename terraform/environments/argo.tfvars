# Environment/Cluster Name
environment = "argocd-test"

# Azure location
location = "northcentralus"

# Tag 'Creator'
creator = "cmaahs@outlook.com"

# Overall CIDR address space for the environment
vnet_cidr = "10.192.192.0/19"

# CIDR for vnet subnet
vnet_subnet_sidr = "10.192.192.0/22"

# Service subnet CIDR for AKS cluster
aks_service_cidr = "10.192.196.0/22"

# DNS service IP address for AKS cluster
aks_dns_service_ip = "10.192.196.10"

# Docker overlay subnet CIDR for AKS cluster
aks_docker_cidr = "10.192.200.1/22"

# Version of Kubernetes to us
kubernetes_version = "1.22.4"

# Public Key for Node Group Nodes
public_ssh_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCtclL1U6LupVdYextshDvDx68RvjtzchSTb5dvRZdiip5XWeQdRlq1pqfJgKOweZZAK5sh2Fftl1c3kkx/Lv6O0IKooyMcZDkmZKTD0F6wrktB/a13wkj1lJ2WpkKEcg4E0x8Z6brMnFd9hhyZaPzBZauHSTsEmkfuVVBDfjSNIruwIv+gaLxPCtpA/kaFJrrrBQrq490CZICJ/7N8KnjkjkfZ7X2SpeN3ngCJ81c/hX6msYixrKjLBvFrcVRmmv96j6OOonPN35DPgRNOo4oTpYWSsNVJMk5zdtV3qZafecvToZFZNG9ExV7STekVo0j1LYgSV1/b8evS0UaFKrZLOz2hJ/8yYJIb/9I1FMK67YuLGfvgSNRVxNvlwAZfkI4YVDhrm4Gnuc6xOwME23iNB9dc1Z2eCVD2/+duwDWVXz/rvbFqnncYIoUpFPErMA3YUnEUyvps4W5Zc0uPUKf1Kf5qz2WAi7q3vUQ67wdOMwcsIqm2qrPCoXfEftgVNTEw1OGgyYy9YUsrk2KsXdz8SffNrO+x51qikO3m5XYIzncksGpECFO3R2u5WdDxtzY0YTdWdWKbF28PVxgfFQ+tKVWjgEzQjC40kxL5XsWjnfrlbecRCcakg1Toj3TbXcwzfEEAs5mzKZDT2XJAnc2sN3jnHUHJ+Kk2NLyvwA8IWQ== chris.maahs@evolutioniq.com"


# Core node pool for the AKS cluster (default value)
aks_core_pool = {
  name      = "core",
  vm_size   = "Standard_D2_v3",
  min_count = 1,
  max_count = 3,
  disk_size = 64
}

# Additional node pools of the AKS cluster (default value)
# aks_node_pools = {
#   meta = {
#     vm_size    = "Standard_D8s_v3",
#     node_count = 1,
#     min_count  = 1,
#     max_count  = 12,
#     disk_size  = 100
#   },
#   db = {
#     vm_size    = "Standard_D8s_v3",
#     node_count = 1,
#     min_count  = 1,
#     max_count  = 12,
#     disk_size  = 100
#   },
#   spark = {
#     vm_size    = "Standard_D16s_v3",
#     node_count = 1,
#     min_count  = 1,
#     max_count  = 12,
#     disk_size  = 1000
#   }
# }

# Name of Azure AD group that should have administrative rights to the AKS cluster
aks_admin_group = "Cloud_Engineering"

# Enable Azure storage logging (disabled by default)
#storage_logging_enabled = true

# Number of days to keep storage logs, if enabled (defaults to 7 days)
#storage_logging_retention = 7

# Number of days to keep Splice DB backups (defaults to 14 days)
# backup_retention = 14
