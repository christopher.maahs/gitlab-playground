# Environment/Cluster Name
environment = "nonprod-splice-test1"

# Azure location
location = "eastus2"

# Tag 'Creator'
creator = "cloud@splicemachine.com"

# Overall CIDR address space for the environment
vnet_cidr = "10.192.192.0/19"

# CIDR for vnet subnet
vnet_subnet_sidr = "10.192.192.0/22"

# Service subnet CIDR for AKS cluster
aks_service_cidr = "10.192.196.0/22"

# DNS service IP address for AKS cluster
aks_dns_service_ip = "10.192.196.10"

# Docker overlay subnet CIDR for AKS cluster
aks_docker_cidr = "10.192.200.1/22"

# Version of Kubernetes to us
kubernetes_version = "1.19.7"

# Public Key for Node Group Nodes
public_key = "ssh-rsa AAAAB....9Jow5"

# Core node pool for the AKS cluster (default value)
#aks_core_pool = {
#  name      = "core",
#  vm_size   = "Standard_D4s_v3",
#  min_count = 1,
#  max_count = 3,
#  disk_size = 100
#}

# Additional node pools of the AKS cluster (default value)
# aks_node_pools = {
#   meta = {
#     vm_size    = "Standard_D8s_v3",
#     node_count = 1,
#     min_count  = 1,
#     max_count  = 12,
#     disk_size  = 100
#   },
#   db = {
#     vm_size    = "Standard_D8s_v3",
#     node_count = 1,
#     min_count  = 1,
#     max_count  = 12,
#     disk_size  = 100
#   },
#   spark = {
#     vm_size    = "Standard_D16s_v3",
#     node_count = 1,
#     min_count  = 1,
#     max_count  = 12,
#     disk_size  = 1000
#   }
# }

# Name of Azure AD group that should have administrative rights to the AKS cluster
aks_admin_group = "Cloud_Engineering"

# Enable Azure storage logging (disabled by default)
#storage_logging_enabled = true

# Number of days to keep storage logs, if enabled (defaults to 7 days)
#storage_logging_retention = 7

# Number of days to keep Splice DB backups (defaults to 14 days)
# backup_retention = 14
