#!/usr/bin/env bash

if [[ -z ${GITLAB_TOKEN} ]]; then
    echo "Please set GITLAB_TOKEN to a valid PAT for Gitlab"
    exit 1
fi

# create user cmaahs
CMAAHS_ID=$(curl -Ls --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
         --header "Content-Type: application/json" \
         --data '{"email": "cmaahs@gmail.com", "name": "Christopher Maahs", "username": "cmaahs", "password": "work2day", "skip_confirmation": "true" }' \
         "https://gitlab.maahsome.rocks/api/v4/users" | jq -r '.id'); echo ${CMAAHS_ID}

# create user PAT: cmaahs
CMAAHS_TOKEN=$(curl -Ls --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
         --header "Content-Type: application/json" \
         --data "{
                 \"name\": \"cmaahs-rt-pat\",
                 \"scopes\": [
                    \"api\",
                    \"read_user\",
                    \"read_api\",
                    \"read_repository\",
                    \"write_repository\",
                    \"read_registry\",
                    \"write_registry\"
                 ]
             }" "https://gitlab.maahsome.rocks/api/v4/users/${CMAAHS_ID}/personal_access_tokens" | jq -r '.token'); echo ${CMAAHS_TOKEN}

EVOIQ_BW_FOLDER_ID=$(cat ~/.config/evoiq/bw/evoiq-folder)
BW_ID=$(bw get item "gitlab-argocd-cmaahs-pat" | jq -r '.id'); echo ${BW_ID}
REV_DATE=$(bw get item ${BW_ID} | jq ".login.password = \"${CMAAHS_TOKEN}\"" | bw encode | bw edit item ${BW_ID} | jq -r '.revisionDate'); echo ${REV_DATE}

# add ssh key to :cmaahs
SSH_KEY=$(cat ~/.ssh/id_rsa_cmaahs-evoiq.pub)
KEY_TITLE=$(curl -Ls --request POST --header "PRIVATE-TOKEN: ${CMAAHS_TOKEN}" \
         --header "Content-Type: application/json" \
         --data "{\"title\": \"cmaahs-ssh-key\", \"key\": \"${SSH_KEY}\" }" \
         "https://gitlab.maahsome.rocks/api/v4/user/keys" | jq -r '.title'); echo ${KEY_TITLE}

# create evoiq group
EVOIQ_ID=$(curl -Ls --request POST --header "PRIVATE-TOKEN: ${CMAAHS_TOKEN}" \
         --header "Content-Type: application/json" \
         --data '{"path": "evolutioniq", "name": "evolutioniq" }' \
         "https://gitlab.maahsome.rocks/api/v4/groups/" | jq -r '.id'); echo ${EVOIQ_ID}
# create evoiq/helm group
EVOIQ_HELM_ID=$(curl -Ls --request POST --header "PRIVATE-TOKEN: ${CMAAHS_TOKEN}" \
         --header "Content-Type: application/json" \
         --data "{\"path\": \"helm\", \"name\": \"helm\", \"parent_id\": \"${EVOIQ_ID}\" }" \
         "https://gitlab.maahsome.rocks/api/v4/groups/" | jq -r '.id'); echo ${EVOIQ_HELM_ID}

# create helm/argocd project
EVOIQ_HELM_ARGOCD_ID=$(curl -Ls --request POST --header "PRIVATE-TOKEN: ${CMAAHS_TOKEN}" \
         --header "Content-Type: application/json" \
         --data "{
                 \"path\": \"argocd\",
                 \"default_branch\": \"main\",
                 \"initialize_with_readme\": \"true\",
                 \"visibility\": \"internal\",
                 \"namespace_id\": \"${EVOIQ_HELM_ID}\"
             }" "https://gitlab.maahsome.rocks/api/v4/projects" | jq -r '.id'); echo ${EVOIQ_HELM_ARGOCD_ID}

# create evoiq/deployments group
EVOIQ_DEPLOYMENTS_ID=$(curl -Ls --request POST --header "PRIVATE-TOKEN: ${CMAAHS_TOKEN}" \
         --header "Content-Type: application/json" \
         --data "{\"path\": \"deployments\", \"name\": \"deployments\", \"parent_id\": \"${EVOIQ_ID}\" }" \
         "https://gitlab.maahsome.rocks/api/v4/groups/" | jq -r '.id'); echo ${EVOIQ_DEPLOYMENTS_ID}

# create evoiq/deployments/evoiq-applications
EVOIQ_DEPLOYMENTS_APPS_ID=$(curl -Ls --request POST --header "PRIVATE-TOKEN: ${CMAAHS_TOKEN}" \
         --header "Content-Type: application/json" \
         --data "{\"path\": \"evoiq-applications\", \"name\": \"evoiq-applications\", \"parent_id\": \"${EVOIQ_DEPLOYMENTS_ID}\" }" \
         "https://gitlab.maahsome.rocks/api/v4/groups/" | jq -r '.id'); echo ${EVOIQ_DEPLOYMENTS_APPS_ID}

# create evoiq/deployments/evoiq-applications/{environment/ring} project
EVOIQ_DEPLOYMENTS_APPS_RING_ID=$(curl -Ls --request POST --header "PRIVATE-TOKEN: ${CMAAHS_TOKEN}" \
         --header "Content-Type: application/json" \
         --data "{
                 \"path\": \"dev\",
                 \"default_branch\": \"main\",
                 \"initialize_with_readme\": \"true\",
                 \"visibility\": \"internal\",
                 \"namespace_id\": \"${EVOIQ_DEPLOYMENTS_APPS_ID}\"
             }" "https://gitlab.maahsome.rocks/api/v4/projects" | jq -r '.id'); echo ${EVOIQ_DEPLOYMENTS_APPS_RING_ID}

